# wreiner.at Theme

## Get current settings
For Cinnamon run:

```
gsettings list-recursively > cin.$(date '+%Y%m%d')
```

## Pluto

```
pacin faenza-icon-theme
yay -S aur/faience-themes

## theme and panel settings
gsettings set org.cinnamon cinnamon-settings-advanced true
gsettings set org.cinnamon desktop-layout 'classic'
gsettings set org.cinnamon workspace-expo-view-as-grid true
gsettings set org.cinnamon.desktop.wm.preferences num-workspaces 8

gsettings set org.cinnamon enabled-applets "['panel1:right:6:systray@cinnamon.org:0', 'panel1:left:0:menu@cinnamon.org:1', 'panel1:left:1:panel-launchers@cinnamon.org:3', 'panel2:left:2:window-list@cinnamon.org:4', 'panel1:right:7:keyboard@cinnamon.org:5', 'panel1:right:8:notifications@cinnamon.org:6', 'panel1:right:9:removable-drives@cinnamon.org:7', 'panel1:right:10:user@cinnamon.org:8', 'panel1:right:11:network@cinnamon.org:9', 'panel1:right:12:power@cinnamon.org:11', 'panel1:right:13:calendar@cinnamon.org:12', 'panel2:right:3:sound@cinnamon.org:13', 'panel1:right:5:ScreenShot@tech71:16', 'panel1:right:4:xrandr@cinnamon.org:17', 'panel2:left:1:workspace-switcher@cinnamon.org:18', 'panel2:left:0:show-desktop@cinnamon.org:21', 'panel1:right:3:pomodoro@gregfreeman.org:22', 'panel1:left:2:places-bookmarks@dmo60.de:24', 'panel1:right:0:panel-launchers@cinnamon.org:25']"

gsettings set org.gnome.desktop.background show-desktop-icons true
gsettings set org.gnome.desktop.background picture-uri 'file:///usr/share/backgrounds/x250-exploded.png'
gsettings set org.gnome.desktop.interface buttons-have-icons true
gsettings set org.gnome.desktop.interface clock-show-date true
gsettings set org.gnome.desktop.interface gtk-theme 'Faience'
gsettings set org.gnome.desktop.interface menus-have-icons true
gsettings set org.gnome.desktop.wm.preferences audible-bell false
gsettings set org.gnome.desktop.wm.preferences mouse-button-modifier '<Alt>'
gsettings set org.gnome.desktop.wm.preferences theme 'Faience'
gsettings set org.nemo.desktop show-desktop-icons false

# panel height
gsettings set org.cinnamon panels-height "['1:25', '2:25']"

# smaller icons
gsettings set org.cinnamon.theme symbolic-relative-size 0.67000000000000004
gsettings set org.cinnamon panel-zone-icon-sizes '[{"panelId": 1, "left": 0, "center": 0, "right": 16}, {"panelId": 2, "left": 16, "center": 16, "right": 22}]'

# set values for panels
gsettings set org.cinnamon panels-autohide "['1:false', '2:false']"
gsettings set org.cinnamon panels-hide-delay "['1:0', '2:0']"

## cinnamon keyboard shortcuts
# switch to workspace
gsettings set org.cinnamon.desktop.keybindings.wm switch-to-workspace-7 "['<Primary><Alt>7']"
gsettings set org.cinnamon.desktop.keybindings.wm switch-to-workspace-8 "['<Primary><Alt>8']"
gsettings set org.cinnamon.desktop.keybindings.wm switch-to-workspace-1 "['<Primary><Alt>1']"
gsettings set org.cinnamon.desktop.keybindings.wm switch-to-workspace-2 "['<Primary><Alt>2']"
gsettings set org.cinnamon.desktop.keybindings.wm switch-to-workspace-3 "['<Primary><Alt>3']"
gsettings set org.cinnamon.desktop.keybindings.wm switch-to-workspace-4 "['<Primary><Alt>4']"
gsettings set org.cinnamon.desktop.keybindings.wm switch-to-workspace-5 "['<Primary><Alt>5']"
gsettings set org.cinnamon.desktop.keybindings.wm switch-to-workspace-6 "['<Primary><Alt>6']"

# move window to workspace
gsettings set org.cinnamon.desktop.keybindings.wm move-to-workspace-6 "['<Alt>6']"
gsettings set org.cinnamon.desktop.keybindings.wm move-to-workspace-7 "['<Alt>7']"
gsettings set org.cinnamon.desktop.keybindings.wm move-to-workspace-8 "['<Alt>8']"
gsettings set org.cinnamon.desktop.keybindings.wm move-to-workspace-1 "['<Alt>1']"
gsettings set org.cinnamon.desktop.keybindings.wm move-to-workspace-2 "['<Alt>2']"
gsettings set org.cinnamon.desktop.keybindings.wm move-to-workspace-3 "['<Alt>3']"
gsettings set org.cinnamon.desktop.keybindings.wm move-to-workspace-4 "['<Alt>4']"
gsettings set org.cinnamon.desktop.keybindings.wm move-to-workspace-5 "['<Alt>5']"

# launch terminator as terminal
gsettings set org.cinnamon.desktop.default-applications.terminal exec-arg '-x'
gsettings set org.cinnamon.desktop.default-applications.terminal exec 'terminator'

## Enable eurkey layout
gsettings set org.gnome.libgnomekbd.keyboard layouts "['eu', 'at']"

## Switch keyboard layout by holding capslock
gsettings set org.gnome.libgnomekbd.keyboard options "['grp\tgrp:caps_switch']"

## screensaver, power and locking
# suspend on lid close and lock
gsettings set org.cinnamon.settings-daemon.plugins.power lid-close-ac-action 'suspend'
gsettings set org.cinnamon.settings-daemon.plugins.power lock-on-suspend true

# suspend on power button
gsettings set org.cinnamon.settings-daemon.plugins.power button-power 'suspend'

# screensaver message
gsettings set org.cinnamon.desktop.screensaver default-message 'wreiner.at - relaying useless spam since 2005-07-15 ..'
gsettings set org.cinnamon.desktop.screensaver show-clock true

## nemo settings
gsettings set org.nemo.list-view default-visible-columns "['name', 'size', 'type', 'date_modified', 'group', 'owner', 'permissions']"
gsettings set org.nemo.list-view default-column-order "['name', 'size', 'type', 'date_modified', 'date_created_with_time', 'date_accessed', 'date_created', 'detailed_type', 'group', 'where', 'mime_type', 'date_modified_with_time', 'octal_permissions', 'owner', 'permissions']"
gsettings set org.nemo.preferences default-folder-viewer 'list-view'

```

## Links
- [HowTo create ThinkPad wallpapers (via reddit.com)](https://imgur.com/a/i1ASd)
